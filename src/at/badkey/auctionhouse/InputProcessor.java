package at.badkey.auctionhouse;

import at.badkey.auctions.Auction;
import at.badkey.bidder.Bidder;
import at.badkey.bidder.BidderManager;

public class InputProcessor {

	public static String input(String input){
		//Example: 1,A,5,B,10,A,8,A,17,B,17
		
		String[] split = input.split(",");
		
		Auction a = new Auction(Integer.parseInt(split[0]));
		
		for(int i = 0; i < split.length; i += 2) {
			if(i==0) continue;
			Bidder b = BidderManager.addBidder(split[i-1], Integer.parseInt(split[i]));
			a.updateBidder(b);
			
			System.out.println(split[i-1]+Integer.parseInt(split[i]));
			
		}
		
		return a.getHistory();
		
		
	}

}
