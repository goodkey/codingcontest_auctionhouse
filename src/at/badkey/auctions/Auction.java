package at.badkey.auctions;

import at.badkey.bidder.Bidder;
import at.badkey.bidder.BidderManager;

public class Auction {

	private int sb = 1;
	private int bid = 1;
	private String bidLeaderID = null;
	private String history = "-,";

	public Auction(int sb) {
		this.sb = sb;
		bid = sb;
		history += sb + ",";
	}

	public void updateBidder(Bidder b) {
		if (b.getMaxBid() == this.sb) {
			bidLeaderID = b.getId();
			history += bidLeaderID + "," + bid + ",";
		} else if (b.getMaxBid() < BidderManager.getBidderByID(bidLeaderID).getMaxBid() && b.getMaxBid() > bid) {
			bid = b.getMaxBid() + 1;
			history += bidLeaderID + "," + bid + ",";
		} else if (b.getMaxBid() > BidderManager.getBidderByID(bidLeaderID).getMaxBid() && b.getMaxBid() > bid) {
			bid = BidderManager.getBidderByID(bidLeaderID).getMaxBid() + 1;
			bidLeaderID = b.getId();
			history += bidLeaderID + "," + bid + ",";
		} else if (b.getMaxBid() == BidderManager.getBidderByID(bidLeaderID).getMaxBid()) {
			bid = b.getMaxBid();
			history += bidLeaderID + "," + bid + ",";
		}

	}

	public String getHistory() {

		return history;
	}

}
