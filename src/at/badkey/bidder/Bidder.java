package at.badkey.bidder;

public class Bidder {
	
	private String identifier;
	private int maxBid;
	
	
	public Bidder(String id, int mb) {
		this.identifier = id;
		this.maxBid = mb;
	}
	
	
	public int getMaxBid() {
		return this.maxBid;
	}
	
	public void setMaxBid(int b) {
		this.maxBid = b;
	}
	
	public String getId() {
		return this.identifier;
	}
}
