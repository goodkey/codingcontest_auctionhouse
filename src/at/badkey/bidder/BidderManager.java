package at.badkey.bidder;

import java.util.ArrayList;

public class BidderManager {
	
	public static ArrayList<Bidder> bidderList = new ArrayList<Bidder>();
	
	public static Bidder addBidder(String id, int bid) {
		if(getBidderByID(id) != null) {
			Bidder b = getBidderByID(id);
			b.setMaxBid(bid);
			return b;
		}else {
			Bidder temp = new Bidder(id,bid);
			bidderList.add(temp);
			return temp; 
		}
		
	}
	
	public static Bidder getBidderByID(String id) {
		for(Bidder b : bidderList) {
			if(b.getId().equals(id)) {
				return b;
			}
		}
		return null;
	}

}
